const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Product = require('../models/Product');

const config = require('../config');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

const createRouter = (db) => {
  // Product index
  router.get('/', (req, res) => {
    Product.find().populate('category')
      .then(results => res.send(results))
      .catch(() => res.sendStatus(500));
  });

  // Product create
  router.post('/', upload.single('image'), (req, res) => {
    const productData = req.body;

    if (req.file) {
      productData.image = req.file.filename;
    } else {
      productData.image = null;
    }

    const product = new Product(productData);

    product.save()
      .then(result => res.send(result))
      .catch(error => res.status(400).send(error));
  });

  // Product get by ID
  router.get('/:id', (req, res) => {
    const id = req.params.id;
    db.collection('products')
      .findOne({_id: new ObjectId(req.params.id)})
      .then(result => {
        if (result) res.send(result);
        else res.sendStatus(404);
      })
      .catch(() => res.sendStatus(500));
  });

  router.delete('/:id', (req, res) => {
    Product.findByIdAndRemove(req.params.id, (err, todo) => {
      if (err) return res.status(500).send(err);
      const response = {
        message: "Item successfully deleted",
        id: todo._id
      };
      return res.status(200).send(response);
    });
  });

  return router;
};

module.exports = createRouter;