const express = require('express');
const Category = require('../models/Category');
const Product = require('../models/Product');

const createRouter = () => {
  const router = express.Router();

  router.get('/', (req, res) => {
    Category.find()
      .then(categories => res.send(categories))
      .catch(() => res.sendStatus(500));
  });

  router.get('/:id', (req, res) => {
    Product.find({category: req.params.id})
      .then(categories => res.send(categories))
      .catch(() => res.sendStatus(500));
  });

  router.post('/', (req, res) => {
    const category = new Category(req.body);

    category.save()
      .then(category => res.send(category))
      .catch(error => res.status(400).send(error));
  });

  return router;
};

module.exports = createRouter;