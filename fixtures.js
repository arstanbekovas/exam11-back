const mongoose = require('mongoose');
const config = require('./config');

const Category = require('./models/Category');
const Product = require('./models/Product');

mongoose.connect(config.db.url + '/' + config.db.name);

const db = mongoose.connection;

db.once('open', async () => {
  try {
    await db.dropCollection('categories');
    await db.dropCollection('products');
  } catch (e) {
    console.log('Collections were not present, skipping drop...');
  }

  const [computersCategory, carsCategory] = await Category.create({
    title: 'Computers',
    description: 'Brand new computers'
  }, {
    title: 'Cars',
    description: 'Brand new cars'
  }, {
    title: 'Others',
    description: 'Something others'
  });

  await Product.create({
    title: 'Intel Core i7',
    price: 300,
    description: 'Very cool processor',
    category: computersCategory._id,
    image: 'cpu.jpg'
  }, {
    title: 'Seagate 3TB',
    price: 110,
    desciption: 'Some kinda description',
    category: computersCategory._id,
    image: 'hdd.jpg'
  }, {
    title: 'BMW X5',
    price: 8110,
    desciption: 'Some kinda description',
    category: carsCategory._id,
    image: 'bmv.jpg'
  }, {
    title: 'Lexus RX350',
    price: 3210,
    desciption: 'Some kinda description',
    category: carsCategory._id,
    image: 'lexus.jpg'
  });

  db.close();
});